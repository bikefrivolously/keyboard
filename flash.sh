#!/bin/bash

LAYOUT="BPbzB"

TEMP=$(mktemp --tmpdir moonlander-$(date +%Y%m%d).XXX.bin)
curl -L -o "$TEMP" https://oryx.zsa.io/$LAYOUT/latest/binary
wally-cli "$TEMP"