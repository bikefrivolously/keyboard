# keyboard

## Moonlander
Install go  
`sudo tar -C /usr/local -xzf go1.20.linux-amd64.tar.gz`  
Add /usr/local/go/bin to PATH


Install wally-cli https://github.com/zsa/wally-cli  
`GOBIN=~/.local/bin go install github.com/zsa/wally-cli@latest`


flash.sh downloads the latest version of a layout from oryx and flashes it using wally-cli